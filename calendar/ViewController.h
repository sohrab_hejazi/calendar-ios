//
//  ViewController.h
//  calendar
//
//  Created by Developer on 5/23/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (retain,nonatomic) IBOutlet UILabel *fetchCal;
@property (retain, nonatomic) IBOutlet UITextField *EVENT;
@property (retain, nonatomic) IBOutlet UIButton *changeButtm;

-(IBAction)createCalendar1:(id)sender;
-(IBAction)createCalendar2:(id)sender;
-(IBAction)createCalendar3:(id)sender;
-(IBAction)exit:(id)sender;
-(IBAction)fetch:(id)sender;
-(IBAction)changeEvent:(id)sender;
-(IBAction)calculateTimeChanges:(id)sender;
@end
