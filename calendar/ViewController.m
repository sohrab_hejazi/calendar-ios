//
//  ViewController.m
//  calendar
//
//  Created by Developer on 5/23/13.
//  Copyright (c) 2013 Developer. All rights reserved.
//
#import <EventKit/EventKit.h>
#import "ViewController.h"
#import "AppDelegate.h"
 NSString *             calendarId1;
 NSString *             calendarId2;
 NSString *             calendarId3;
NSString * selectedCalendar;
NSString *eventsStr;
NSArray *events;
NSArray *eventsIDs;
NSString *myID;
static EKEventStore *eventStore = nil;

@interface ViewController ()
+(void)requestAccess:(void (^)(BOOL granted, NSError *error))success;
+(BOOL)addEventAt:(NSDate*)eventDate withTitle:(NSString*)title inLocation:(NSString*)location;
@end

@implementation ViewController
@synthesize fetchCal;
@synthesize EVENT;
@synthesize changeButtm;

-(void) calculateTimeChanges:(id)sender{
    
    
///////////////////////////////////////////////////////////////////////////////
//               fetch all events of local calculator
//////////////////////////////////////////////////////////////////////////////
    EKEventStore *store = [[EKEventStore alloc] init];
    EKCalendar *calendar ;
    if ([selectedCalendar isEqual:@"One"]) {
        calendar = [store calendarWithIdentifier:calendarId1];
    } else if([selectedCalendar isEqual:@"Two"]){
        calendar = [store calendarWithIdentifier:calendarId2];
    } else calendar = [store calendarWithIdentifier:calendarId3];
    NSDate *start =[NSDate date];
    NSDate *finish = [NSDate dateWithTimeIntervalSinceNow:8640000];
    NSArray *calendarArray = [NSArray arrayWithObject:calendar];
    NSPredicate *predicate = [store predicateForEventsWithStartDate:start
                                                            endDate:finish
                                                          calendars:calendarArray];
       NSArray *newEventsArr = [store eventsMatchingPredicate:predicate];
    //NSLog( @"%@", newEventsArr);
    
////////////////////////////////////////////////////////////////////////////// 
//               Read the events' file
/////////////////////////////////////////////////////////////////////////////
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory()
                                    stringByAppendingPathComponent:@"Documents"];
    NSString *filePath = [documentsDirectory
                          stringByAppendingPathComponent:@"eventsFile"];
    //NSArray *newEventsArr=[NSArray arrayWithContentsOfFile:filePath];
    eventsStr = [NSString stringWithContentsOfFile: filePath];
    NSArray *oldEventsArr = [eventsStr componentsSeparatedByString:@"\n"];
    //NSLog( @"%@", oldEventsArr);
    
////////////////////////////////////////////////////////////////////////////
//                 compare the events
////////////////////////////////////////////////////////////////////////////
//bool x=true;
//while (x) {
    int k=[newEventsArr count];
    int m=[oldEventsArr count];
    for (int i=0; i<k; i++) {
        EKEvent *currentEvent=[newEventsArr objectAtIndex:i];
        NSString *idEV=currentEvent.eventIdentifier;
        
        
        //for (EKEvent *backupEvent in oldEventsArr)
        for(int j=0; j<m; j++)
        {
            NSString *backupEvent=[oldEventsArr objectAtIndex:j];
            
            if ([backupEvent isEqualToString: currentEvent.title])
            {
                NSUInteger index = [oldEventsArr indexOfObject:backupEvent];
                
                 NSString *A=currentEvent.location;
                 NSString *B=[oldEventsArr objectAtIndex:index+2];
                //NSDate *c= currentEvent.startDate;
                NSString *startStr1 = [NSDateFormatter localizedStringFromDate:currentEvent.startDate
                                                                    dateStyle:NSDateFormatterShortStyle
                                                                    timeStyle:NSDateFormatterFullStyle];
                
                NSString *startStr2 = [oldEventsArr objectAtIndex:index+3];
                NSString *endStr1 = [NSDateFormatter localizedStringFromDate:currentEvent.endDate
                                                                     dateStyle:NSDateFormatterShortStyle
                                                                     timeStyle:NSDateFormatterFullStyle];
                
                NSString *endStr2 = [oldEventsArr objectAtIndex:index+4];
                
                if(![A isEqualToString: B] || ![currentEvent.notes isEqualToString: [oldEventsArr objectAtIndex:index+6]]  || [startStr1 isEqualToString: startStr2] || [endStr1 isEqualToString: endStr2])
                
                    NSLog(@"%@ IS CHANGED",currentEvent.title );
            }
        } 
    
    }
        
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
    NSLog(@"Execution Time to compare: %f", executionTime);
        
//start =[NSDate date];}
}

-(void)changeEvent:(id)sender{
    EKEventStore *store = [[EKEventStore alloc] init];
    EKCalendar *calendar ;
    if ([selectedCalendar isEqual:@"One"]) {
        calendar = [store calendarWithIdentifier:calendarId1];
    } else if([selectedCalendar isEqual:@"Two"]){
        calendar = [store calendarWithIdentifier:calendarId2];
    } else calendar = [store calendarWithIdentifier:calendarId3];
    //EKEvent *events = [store eventWithIdentifier:EVENT.text];
    NSInteger myIndex = [EVENT.text intValue];
    NSString *myEVENT=[NSString stringWithFormat:@"event%@", EVENT.text ];
                      
                      
    for (EKEvent *eventToChange in events)
    {
        if ([eventToChange.title isEqualToString: myEVENT])
             {
                 myID =[eventsIDs objectAtIndex:myIndex*2+1 ];
                 EKEvent *Event = [store eventWithIdentifier:myID];
                 
                 NSError *err;
                 BOOL success = [store removeEvent:Event span:EKSpanThisEvent commit:YES error:&err];
                 //eventToChange.location=@"white hause";
                 
                 //[store saveCalendar:calendar commit:YES error:&err];
                 
                  //if (!err)
                 //NSLog(@"event changed success ");
                 //break;
                 //EKEvent *Event = [store eventWithIdentifier:eventToChange.eventIdentifier];
                 
               
                 EKEvent *event = [EKEvent eventWithEventStore:store];
                 //event.eventIdentifier=eventToChange.eventIdentifier;
                 event.calendar = calendar;
                 event.location = @"VIENNA Austria";
                 event.title=eventToChange.title;
                 event.startDate = eventToChange.startDate ;
                 event.endDate = eventToChange.endDate;
                 
                 event.notes=@"hello sohrab";
                 NSError *error = nil;
                 // save event to the callendar
                 BOOL result = [store saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
                 NSLog(@"event changed success"); 
             }
    }
        
    changeButtm.hidden=NO;
}



-(void)fetch:(id)sender{
    
    EKEventStore *store = [[EKEventStore alloc] init];
    EKCalendar *calendar ;
    if ([selectedCalendar isEqual:@"One"]) {
      calendar = [store calendarWithIdentifier:calendarId1];
    } else if([selectedCalendar isEqual:@"Two"]){
       calendar = [store calendarWithIdentifier:calendarId2];
    } else calendar = [store calendarWithIdentifier:calendarId3];
    
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        // handle access here
    }];
    
    
    // Get the appropriate calendar
    //NSCalendar *calendar = [NSCalendar currentCalendar];
    
    // Create the start date components
    NSDate *start =[NSDate date];
    NSDate *finish = [NSDate dateWithTimeIntervalSinceNow:8640000];
    // Create the predicate from the event store's instance method
    NSArray *calendarArray = [NSArray arrayWithObject:calendar];
    NSPredicate *predicate = [store predicateForEventsWithStartDate:start
                                                            endDate:finish
                                                          calendars:calendarArray];
    
    // Fetch all events that match the predicate
    events = [store eventsMatchingPredicate:predicate];
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
    
    NSLog(@"Execution Time to fetch: %f", executionTime);
    NSLog(@"fetched all events");
    //NSLog( @"%@", events);
    
    eventsStr= [events componentsJoinedByString:@"\n"];
    //NSLog( @"%@", events);
    
    
    
}

+ (void)requestAccess:(void (^)(BOOL granted, NSError *error))callback;
{
    if (eventStore == nil) {
        eventStore = [[EKEventStore alloc] init];
    }
    // request permissions
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:callback];
}

+ (BOOL)addEventAt:(NSDate*)eventDate withTitle:(NSString*)title inLocation:(NSString*)location calendarTitle: (NSString*) calendarName id: (NSString*)ID
{
        EKCalendar *calendar = nil;
    NSString *calendarIdentifier = [[NSUserDefaults standardUserDefaults] valueForKey:ID];
    //AppDelegate->idOne= calendarIdentifier ;
    // when identifier exists, my calendar probably already exists
    // note that user can delete my calendar. In that case I have to create it again.
    if (calendarIdentifier) {
        calendar = [eventStore calendarWithIdentifier:calendarIdentifier];
        NSLog(@"calendar number %@ already exists", ID);
    }
    
    
    // calendar doesn't exist, create it and save it's identifier
    if (!calendar) {
        
        calendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:eventStore];
        NSLog(@"created a new calendar number %@", ID);
        calendar.title=calendarName;
        
        //calendarIdentifier = [[NSUserDefaults standardUserDefaults] valueForKey:ID];
        for (EKSource *s in eventStore.sources) {
            if (s.sourceType == EKSourceTypeLocal) {
                calendar.source = s;
                NSLog(@"Local source calendar exists");                  
                break;
            }
        }
                // save this in NSUserDefaults data for retrieval later
        NSString *calendarIdentifier = [calendar calendarIdentifier];
        
        NSError *error = nil;
        BOOL saved = [eventStore saveCalendar:calendar commit:YES error:&error];
        if (saved) {
                    [[NSUserDefaults standardUserDefaults] setObject:calendarIdentifier forKey:ID];
        } else {
            // unable to save calendar
            return NO;
        }
    }
    
    
    
    
    if ([ID isEqual:@"One"]) {
        calendarId1=calendar.calendarIdentifier;
    } else if([ID isEqual:@"Two"]){
        calendarId2=calendar.calendarIdentifier;
    } else calendarId3=calendar.calendarIdentifier;
    
    
    // this shouldn't happen
    if (!calendar) {
        return NO;
    }
    int i,j,k,m;
    if ([ID isEqual:@"One"]) {
        k=3;
    } else if([ID isEqual:@"Two"]){
        k=6;
    } else k=10;
    // assign basic information to the event; location is optional
    m=0;
    NSString *eventId;
    NSString *eventsInformation;
    //NSArray *eventsInformationArr;
    NSDate *start=[NSDate date];
    for (i=0; i<100; i++) {
        for (j=0; j<k; j++) {
            
            EKEvent *event = [EKEvent eventWithEventStore:eventStore];
            event.calendar = calendar;
            event.location = location;
            NSString *t = [NSString stringWithFormat:@"%@%d", title, m];
            event.title = t;
            //NSDateComponents *DayCr = [[NSDateComponents alloc] init];
            //NSDate *startDate ;
            //DayCremental.day = 1;
            int day=(86400*i)+(7200*j);
            
            
            //NSLog([NSString stringWithFormat:@"%d",day]);
            event.startDate = [start dateByAddingTimeInterval:day];
            event.endDate = [start dateByAddingTimeInterval:day+3600];
            //startDate.date++;
            event.notes=@"hello world";
            m++;
            NSError *error = nil;
            // save event to the callendar
            BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
            eventId = [NSString stringWithFormat:@"%@%@\n%@\n",eventId, t, event.eventIdentifier];
            
            eventsInformation = [NSString stringWithFormat:@"%@%@\n%@\n%@\n%@\n%@\n%@\n%@\n",eventsInformation, t, event.eventIdentifier,event.location,event.startDate,event.endDate,event.attendees,event.notes];
        }
    }
    eventsIDs = [eventId componentsSeparatedByString:@"\n"];
    //eventsInformationArr= [eventsInformation componentsSeparatedByString:@"\n"];
    NSError *error;
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSString *documentsDirectory = [NSHomeDirectory()
                                    stringByAppendingPathComponent:@"Documents"];
    NSString *filePath = [documentsDirectory
                          stringByAppendingPathComponent:@"eventsFile"];
    [eventsInformation writeToFile:filePath atomically:YES];
    // Show contents of Documents directory
    NSLog(@"Documents directory: %@",
          [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
    
    
}

-(void) exit:(id)sender{
    
    EKEventStore *eventStore1 = [[EKEventStore alloc] init];
    EKCalendar *calendar1 = [eventStore1 calendarWithIdentifier:calendarId1];
    if (calendar1) {
        NSError *error = nil;
        BOOL result = [eventStore1 removeCalendar:calendar1 commit:YES error:&error];
        if (result) {
            NSLog(@"Deleted calendar one from event store.");
        } else {
            NSLog(@"Deleting calendar failed: %@.", error);
        }
    }
    EKEventStore *eventStore2 = [[EKEventStore alloc] init];
    EKCalendar *calendar2 = [eventStore2 calendarWithIdentifier:calendarId2];
    if (calendar2) {
        NSError *error = nil;
        BOOL result = [eventStore2 removeCalendar:calendar2 commit:YES error:&error];
        if (result) {
            NSLog(@"Deleted calendar two from event store.");
        } else {
            NSLog(@"Deleting calendar failed: %@.", error);
        }
    }
    
        EKEventStore *eventStore3 = [[EKEventStore alloc] init];
        EKCalendar *calendar3 = [eventStore3 calendarWithIdentifier:calendarId3];
        if (calendar3) {
            NSError *error = nil;
            BOOL result = [eventStore3 removeCalendar:calendar3 commit:YES error:&error];
            if (result) {
                NSLog(@"Deleted calendar Three from event store.");
            } else {
                NSLog(@"Deleting calendar failed: %@.", error);
            }
            
        }
    
    
   /* EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKEvent *event = [EKEvent eventWithEventStore:eventStore];
    EKCalendar *calendar = [eventStore calendarWithIdentifier:_calendarId1];
    event.calendar = calendar;
    
    // Set the start date to the current date/time and the event duration to one hour
    NSDate *startDate = [NSDate date];
    event.startDate = startDate;
    event.endDate = [startDate dateByAddingTimeInterval:3600];
    
    NSError *error = nil;
    BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
    if (result) {
        NSLog(@"Saved event to event store.");
    } else {
        NSLog(@"Error saving event: %@.", strerror);
    }
    */
}

-(void) createCalendar1:(id)sender{
    
    selectedCalendar=@"One";
    [ViewController requestAccess:^(BOOL granted, NSError *error) {
        if (granted) {
            BOOL result = [ViewController addEventAt:[NSDate date] withTitle:@"event" inLocation:@"My house" calendarTitle: @"calendar#1" id: @"One"];
            if (result) {
                // added to calendar
                NSLog(@"events added to calendar #one");
            } else {
                // unable to create event/calendar
                NSLog(@"unable to create event in calendar #one");
            }
        } else {
            // you don't have permissions to access calendars
        }
    }];
    fetchCal.text=@"Fetch all Events of Cal. 1";    
}

-(void) createCalendar2:(id)sender{
    
    selectedCalendar=@"Two";
    [ViewController requestAccess:^(BOOL granted, NSError *error) {
        if (granted) {
            BOOL result = [ViewController addEventAt:[NSDate date] withTitle:@"event" inLocation:@"My house" calendarTitle: @"calendar#2" id: @"Two" ];
            if (result) {
                // added to calendar
                NSLog(@"events added to calendar #two");
            } else {
                // unable to create event/calendar
                NSLog(@"unable to create event in calendar #two");
            }
        } else {
            // you don't have permissions to access calendars
        }
    }];
    fetchCal.text=@"Fetch all Events of Cal. 2";    
}

-(void) createCalendar3:(id)sender{
    
    selectedCalendar=@"Three";
    [ViewController requestAccess:^(BOOL granted, NSError *error) {
        if (granted) {
            BOOL result = [ViewController addEventAt:[NSDate date] withTitle:@"event" inLocation:@"My house" calendarTitle: @"calendar#3" id: @"Three"];
            if (result) {
                // added to calendar
                NSLog(@"events added to calendar #three");
            } else {
                // unable to create event/calendar
                NSLog(@"unable to create event in calendar #three");
            }
        } else {
            // you don't have permissions to access calendars
        }
    }];
    fetchCal.text=@"Fetch all Events of Cal. 3";
   /* EKEventStore *eventStore = [[EKEventStore alloc] init];
    EKCalendar *calendar = [EKCalendar calendarWithEventStore:eventStore];
    calendar.title = @"calendar-1";
    
    // Iterate over all sources in the event store and look for the local source
  EKSource *theSource = nil;
    
    for (EKSource *source in eventStore.sources) {
        if (source.sourceType == EKSourceTypeLocal ) {
            theSource = source;
            break;
        }
    }
    
    if (theSource) {
        calendar.source = theSource;
    } else {
        NSLog(@"Error: Local source not available");
        return;
    }
    
    NSError *error = nil;
    BOOL result = [eventStore saveCalendar:calendar commit:YES error:&error];
    if (result) {
        NSLog(@"Saved calendar to event store, identifier is %@", calendar.calendarIdentifier);
        _calendarId1=calendar.calendarIdentifier;
        //self.calendarIdentifier = calendar.calendarIdentifier;
    } else {
        NSLog(@"Error saving calendar: %@.", error);
    } */
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    changeButtm.hidden=YES;
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
